<!doctype html>

<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=1024" />
    <title>Challenges in the Post-Minkowskian Description of the Gravitational Two-Body Problem</title>
    
    <meta name="description" content="Presentation Slides made with impress.js" />
    <meta name="author" content="Gregor Kaelin" />

	<script type="text/x-mathjax-config">
	  MathJax.Hub.Config({
      "HTML-CSS": {
	  scale: 90,
	  styles: {
      '.MathJax_Display': {
      "margin": "20px 0"
      }
      }
	  },
	  TeX: { extensions: ["color.js"] },
	  });
	</script>
	<script type="text/javascript" src="extras/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

    <link href="css/main.css" rel="stylesheet" />
    <link rel="shortcut icon" href="favicon.png" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  </head>

  <body class="impress-not-supported">

	<div class="fallback-message">
      <p>Your browser <b>doesn't support the features required</b> by impress.js, so you are presented with a simplified version of this presentation.</p>
      <p>For the best experience please use the latest <b>Chrome</b>, <b>Safari</b> or <b>Firefox</b> browser.</p>
	  <p>
		$$
		\definecolor{desyOrange}{RGB}{242,142,0}
		\DeclareMathOperator{\Arcsinh}{arcsinh}
		\DeclareMathOperator{\Arctan}{arctan}
		\DeclareMathOperator{\arcosh}{arcosh}
		\newcommand\dd{{\mathrm d}}
		\newcommand{\bb}{{\mathbf b}}
		\newcommand{\bk}{{\mathbf k}}
		\newcommand{\bl}{{\mathbf l}}
		\newcommand{\bm}{{\mathbf m}}
		\newcommand{\bn}{{\mathbf n}}
		\newcommand{\bp}{{\mathbf p}}
		\newcommand{\bq}{{\mathbf q}}
		\newcommand{\br}{{\mathbf r}}
		\newcommand{\bw}{{\mathbf w}}
		\newcommand{\bx}{{\mathbf x}}
		\newcommand{\by}{{\mathbf y}}
		\newcommand{\bz}{{\mathbf z}}
		\newcommand{\bc}{{\mathbf c}}
		\newcommand{\bell}{\boldsymbol{\ell}}
		\newcommand\cO{\mathcal{O}}
		\newcommand{\cD}{\mathcal{D}}
		\newcommand\cA{\mathcal{A}}
		\newcommand\cM{\mathcal{M}}
		\newcommand\cN{\mathcal{N}}
		\newcommand\cE{\mathcal{E}}
		\newcommand\cS{\mathcal{S}}
		\newcommand\cP{\mathcal{P}}
		\newcommand\cF{\mathcal{F}}
		\newcommand\cL{\mathcal{L}}
		\newcommand\cI{\mathcal{I}}
		\newcommand\cH{\mathcal{H}}
		\newcommand\cU{\mathcal{U}}
		\newcommand\Mp{M_{\rm Pl}}
		$$
	  </p>
	</div>

	<div id="impress">

	  <div id="title" class="step" data-x="0" data-y="0">
		<h1>Challenges in the Post-Minkowskian Description of the Gravitational Tw<span class="orange">o</span>-Body Problem</h1>
		<p class="margin-bottom30">Work with Christoph Dlapa, Zhengwen Liu, <br/>
		  Jakob Neef & Rafael Porto<br/>
		  <br/>
		  <a href="https://arxiv.org/abs/2207.00580">[2207.00580]</a>
		  <br/>
		  <a href="https://arxiv.org/abs/2210.05541">[2210.05541]</a>
		  <br/>
		  <a href="https://arxiv.org/abs/2304.01275">[2304.01275]</a>
		</p>
		<img id="scatteringFig" src="figures/scattering.svg" alt="scattering">
		<img id="orbitFig" src="figures/orbit.svg" alt="orbit">
		<p id="author">
		  Gregor Kälin
		</p>
		
		<div class="margin-top50 margin-bottom30">
		  <img id="ercFig" src="figures/erc.svg" alt="erc">
		  <img id="desyFig" src="figures/logo_DESY.png" alt="desy">
		  <img id="euFig" src="figures/eu.svg" alt="eu">
		</div>
		
		<div>
		  <i>
			Nordita 24.07.2023
		  </i>
		</div>
	  </div>

	   <div id="Motivation" class="step" data-rel-x="1800" data-rel-to="title">
		<p class="center">
		  <blockquote class="callout quote EN">
			I would like to mention astrophysics; in this field, the strange properties of the pulsars and quasars, and perhaps also the gravitational waves, can be considered as a challenge.
			<cite> - Werner Heissenberg</cite>
		  </blockquote>
		</p>
	  </div>

	  <div id="main" class="step" data-rel-x="1700">
		<h2><span class="orange">O</span>verview</h2>
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering"></img>
		</p>
	  </div>

	  <div id="integrand" class="step" data-rel-x="1700">
		<h3><span class="h3span">Challenge 1: Integrand construction</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Efficiently and systematically computable.</li>
			<li>Minimal
			  <ul>
				<li>Free of redundancies.</li>
				<li>Only information that we want.</li>
			  </ul>
			</li>
			<li><q>Optimal</q> representation.</li>
		  </ul>
		</blockquote>
	  </div>

	  <div id="pmeft" class="step" data-rel-x="0" data-rel-y="900">
		<h2>A w<span class="orange">o</span>rldline EFT framework</h2>
		<a class="ref" href="https://arxiv.org/abs/2006.01184">[GK, Porto 2006.01184]</a>
		<ul>
		  <li>Equivalent to solving <em>classical</em> equations of motion.</li>
		  <li>Perturbative expansion in \(G\): particle physics/amplitudes toolbox</li>
		  <li>EFT methodology: Full action \(\rightarrow\) effective action \(\rightarrow\) deflection/fluxes/waveform/...</li>
		  <li>Complete: allows inclusion of radiation, finite size, spin, \(n\)-body</li>
		</ul>
		<p class="center">
		  <img id="setupFig" src="figures/setup.svg" alt="setup"></img>
		</p>
	  </div>
	  
	  <div id="pmeft1.5" class="step" data-rel-x="0" data-rel-y="600">
		<h3><span class="h3span">Full the<span class="orange">o</span>ry</span></h3>
		<p>
		  Model the compact bodies by worldlines \(x_a^\mu(\tau)\) coupled to GR.
	      $$\begin{align}
		  S_{\rm EH} &= -2\Mp^2 \int \dd^4x \sqrt{-g} \, R[g]\\
		  S_{\rm pp} &= -\sum_a \frac{m_a}{2} \int \dd\tau_a\,  g_{\mu\nu}(x_{a}(\tau_a)) \dot{x}_{a}^\mu(\tau_a) \dot{x}_{a}^\nu (\tau_a)+\dots\\
		  \end{align}$$
		  We can add more terms to describe tidal and spin effects in an EFT style.
		</p>
	  </div>

	  <div id="pmeft2" class="step" data-rel-x="0" data-rel-y="580">
		<h3><span class="h3span">Effective two-body acti<span class="orange">o</span>n</span></h3>
		<p>
		  First step: Integrate out the gravitational field (classical saddlepoint).
		  $$g_{\mu\nu}=\eta_{\mu\nu} + \kappa h_{\mu\nu}$$
		  $$e^{i S_{\rm eff}[x_a] } = \int \cD h_{\mu\nu} \, e^{i S_{\rm EH}[h] + i S_{\rm GF}[h] + i S_{\rm TD}[h] + i S_{\rm pp}[x_a,h]}$$
		</p>
		<blockquote class="callout quote EN">
		  Don't use Feynman rules!</br>
		  <cite> - An amplitudist</cite>
		</blockquote>
		<p>
		Optimize the EH-Lagrangian by means of gauge-fixing terms and total derivatives:
		</p>
		<div class="container">
		<ul class="left">
		  <li>2-point Lagrangian: 2 terms</li>
		  <li>3-point Lagrangian: 6 terms</li>
		  <li>4-point Lagrangian: 18 terms</li>
		  <li>5-point Lagrangian: 36 terms</li>
		</ul>
		<img id="notBadFig" class="right" src="figures/notBad.png" alt="notBad"><img>
		</div>
	  </div>

	  <div id="rad" class="step" data-rel-x="0" data-rel-y="750">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n reaction</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2207.00580">[GK, Neef, Porto 2207.00580]</a>
		<ul class="margin-top60">
		  <li>For causal motion: in-in formalism \(\longrightarrow\) doubling of fields </br><a class="inlineRef" href="https://journals.aps.org/prd/abstract/10.1103/PhysRevD.33.444">[Jordan 1986]</a><a class="inlineRef" href="https://journals.aps.org/prd/abstract/10.1103/PhysRevD.35.495">[Calzetta, Hu 1987]</a><a class="inlineRef" href="https://arxiv.org/abs/0903.1122">[Galley, Tiglio 0903.1122]</a>
			$$\begin{align}
			\cS[h_1,h_2] = \cS_\textrm{EH}[h_1] - \cS_\textrm{EH}[h_2] -\sum_{A=1}^2 \frac{\kappa m_A}{2}\int\dd\tau_A &\left[h_{1,\mu\nu}(x_{1,A}(\tau_A))\dot{x}_{1,A}^\mu(\tau_A)\dot{x}_{1,A}^\nu(\tau_A)\right.\\
			&\quad\left.-h_{2,\mu\nu}(x_{2,A}(\tau_A))\dot{x}_{2,A}^\mu(\tau_A)\dot{x}_{2,A}^\nu(\tau_A)\right]
			\end{align}
			$$
		  </li>
		  <li>Rotate to Keldysh variables (advanced & retarded propagators)
			$$\begin{align}
			h_{\mu\nu}^- &= \frac{1}{2}(h_{1\mu\nu}+h_{2\mu\nu}) & h_{\mu\nu}^+ &= h_{1\mu\nu} - h_{2\mu\nu}\\
			x_{a,+}^\mu &= \frac{1}{2}(x_{a,1}^\mu+x_{a,2}^\mu) & x_{a,-}^\mu &= x_{a,1}^\mu - x_{a,2}^\mu
			\end{align}$$
		  </li>
		  <li>Seems impractical, but it's not!</li>
		</ul>
	  </div>

	  <div id="rad2" class="step" data-rel-x="0" data-rel-y="750">
		<ul>
		  <li>Simple Feynman rules for <em>variation</em> of the action (in the physical limit)
			<ul>
			  <li>Feynman\(\rightarrow\)retarded/advanced propagator; graviton vertices unchanged</li>
			  <li>Source:
				<div class="graphsRad">
				  <img id="sourceFig" src="figures/source.svg" alt="source"></img>
				  <div>
					$$ = -\frac{i  m}{2\Mp}\int\dd\tau \, e^{i\, k\cdot x} \dot{x}^\mu \dot{x}^\nu$$
				  </div>
				</div>
			  </li>
			  <li>Unique sink (hit by variation):
				<div class="graphsRad">
				  <img id="sinkFig" src="figures/sink.svg" alt="source"></img>
				  <div>
					$$=-\frac{i m}{2\Mp} \,e^{i\,k\cdot x} \left[ i\, k^\alpha \dot{x}^\mu \dot{x}^\nu-i\,k\cdot \dot{x}\, \eta^{\mu\alpha}\dot{x}^\nu-\eta^{\mu\alpha}\ddot{x}^\nu-i\,k\cdot\dot{x}\, \eta^{\nu\alpha}\dot{x}^\mu-\eta^{\nu\alpha}\ddot{x}^\mu\right]$$
				  </div>
				</div>
			  </li>
			</ul>
		  </li>
		  <li>Variation of the eff. action
			<div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{1PM}=$$
		  </div>
		  <img id="graphsRad1Fig" src="figures/graphsRad1.svg" alt="graphsRad1"/>
    	<div class="marginLeft">
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{2PM}=$$
		  </div>
		  <img id="graphsRad2Fig" src="figures/graphsRad2.svg" alt="graphsRad2"/>
		  </div>
		  <div class="graphsRad">
			<div>
		  $$\left.\frac{\delta\cS_\textrm{eff}[x_+,x_-]}{\delta x_{1,-}^\alpha}\right|_{\substack{x_-\to 0\\x_+\to x}}^\textrm{3PM}=$$
		  </div>
		  <img id="graphsRad3Fig" src="figures/graphsRad3.svg" alt="graphsRad3"/>
		  </div>
		  </li>
		</ul>
	  </div>
	  
	  <div id="pmeft3" class="step" data-rel-x="0" data-rel-y="850">
		<h3><span class="h3span">C<span class="orange">o</span>mputing observables</span></h3>
		Second step: Solve equation of motions
		$$\left.\frac{\delta \cS_{\textrm{eff}}[x_+,x_-]}{\delta x_{b,-}^\mu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}} = 0 \quad\Longleftrightarrow\quad m_b \ddot{x}_b^\mu(\tau) = \left.-\eta^{\mu\nu}\frac{\delta \cS_{\textrm{eff,int}}[x_+,x_-]}{\delta x_{b,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}$$
		perturbatively, expanding around straight lines
		$$
		x_a^\mu(\tau) = b_a + u_a \tau + \sum_{n=1}^\infty G^n \delta^{(n)}x_a^\mu(\tau)
		$$
		Using above trajectories we can e.g. compute the impulse
		$$\Delta p^\mu_1= m_1 \int_{-\infty}^{+\infty}\dd\tau \ddot{x}_1^\mu = - \eta^{\mu\nu}\int_{-\infty}^{+\infty} \dd\tau \left.\frac{\delta \cS_{\textrm{eff,int}}[x_-,x_+]}{\delta x_{1,-}^\nu(\tau)}\right|_{\substack{x_{a,-}\rightarrow 0\\x_{a,+}\rightarrow x_a}}\,,$$
		or the change of the mechanical angular momentum
		$$\Delta J^{\mu\nu}_1 = m_1 \int_{-\infty}^{+\infty} \dd\tau ( x_1^\mu \ddot{x}_1^\nu- \ddot{x}_1^\mu x_1^\nu)\,.$$
	  </div>

	  <div id="integrand2" class="step" data-rel-y="0" data-rel-x="0" data-rel-to="integrand">
	  </div>

	  <div id="integrandEnd" class="step" data-rel-y="0" data-rel-x="0" data-rel-to="integrand">
		<h3><span class="h3span">Challenge 1: Integrand construction</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Efficiently and systematically computable. <b>Yes.</b></li>
			<li>Minimal
			  <ul>
				<li>Free of redundancies. <b>Iterations, on-shellness,...</b></li>
				<li>Only information that we want. <b>Yes.</b></li>
			  </ul>
			</li>
			<li><q>Optimal</q> representation. <b>Probably no.</b></li>
		  </ul>
		</blockquote>
	  </div>

	  <div id="integrals" class="step" data-rel-x="1700">
		<h3><span class="h3span">Challenge 2: (Feynman) Integrals</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Analytic representation.</li>
			<li>vs. fast numerical evaluation.</li>
			<li>Minimal function space.</li>
			<li>Bootstrap.</li>
		  </ul>
		</blockquote>
	  </div>

	  <div id="int1" class="step" data-rel-x="0" data-rel-y="900">
		<h2>Integrati<span class="orange">o</span>n</h2>
		<ul>
		  <li>\(\tau\)-integrations are trivial to resolve: lead to delta functions and linear propagators.</li>
		</ul>
		Generic structure to any loop order (potential + radiation):
		$$
		\int \dd^Dq\frac{\delta(q\cdot u_1)\delta(q\cdot u_2)e^{i b\cdot q}}{(q^2)^m}
		\underbrace{\int \dd^D\ell_1\cdots\dd^D\ell_L\frac{\delta(\ell_1\cdot u_{a_1})\cdots\delta(\ell_L\cdot u_{a_L})}{(\ell_1\cdot u_{b_1}\pm i0)^{i_1}\cdots(\ell_L\cdot u_{b_L}\pm i0)^{i_L}(\textrm{sq. props})}}_{\textrm{Cut Feynman integrals with linear and square propagators}}
		$$
		<ul>
		  <li>Single scale integrals in \(\gamma = u_1\cdot u_2\)</li>
		  <li>We can use all modern integration methods from the amplitudes community</li>
		  <li>One delta function per loop \(\rightarrow\) half of the linear propagators are cut</li>
		  <li>Outer Fourier transform is easy.</li>
		</ul>
	  </div>

	  <div id="int2" class="step" data-rel-y="600" data-rel-x="0">
		<h3><span class="h3span">Integration pr<span class="orange">o</span>cedure</span></h3>
		<ul>
		  <li>Tensor reduction (standard PaVe) \(\rightarrow\) scalar integrals</li>
		  <li>Integration-By-Part (IBP) reduction \(\rightarrow\) master integrals
			<blockquote class="box">
			  <b>Technical challenges:</b>
			  <ul>
				<li>Huge system of equations with many redundancies.</li>
				<li>Lack of easily applicable measure for <q>good</q> choice of masters.</li>
				<li>Symmetries of retarded integrals.</li>
			  </ul>
			</blockquote>
		  </li>
		  <li>Method of differential equations: bring into \(\epsilon\)-form <a class="refInline">[Kotikov, Remiddi, Henn]</a>
			<ul>
			  <li>See <a class="refInline" href="https://arxiv.org/abs/2304.01275">[2304.01275]</a> and newest version of INITIAL <a class="refInline" href="https://arxiv.org/abs/2211.16357">[Dlapa, Henn, Wagner '22]</a></li>
			  
			</ul>
			<blockquote class="box">
			  <b>Technical challenge:</b> Everything more complicated than polylogs.
			</blockquote>
		  </li>
		  <li>Method of regions to compute the boundary constants <a class="refInline">[Beneke, Smirnov]</a></li>
        </ul>
	  </div>

	  <div id="int3" class="step" data-rel-y="650" data-rel-x="0">
		<h3><span class="h3span">Meth<span class="orange">o</span>d of regions @3PM</span></h3>
		<p class="center">
		  <img id="I1fig" src="figures/I1.png"></img>
		</p>
		<p>
		  $$I_1=\int_{\ell_1,\ell_2}\frac{\delta(\ell_1\cdot u_1)\delta(\ell_2\cdot u_2)}{\ell_1^2\ell_2^2(\ell_1+\ell_2-q)^2}$$
		</p>
	  </div>
	  
	  <div id="int5" class="step" data-rel-y="500" data-rel-x="0">
		<hr/>
		<p>
		  Shift \(k\equiv \ell_1+\ell_2-q\) and \(\ell\equiv\ell_2\)
		  $$\begin{align}
		  &\mathrm{potential:} & \ell&\sim (v_\infty,1)|{\bq}|\,, & k&\sim(v_\infty,1)|{\bq}|\,,\\
		  &\mathrm{radiation:} & \ell&\sim (v_\infty,1)|{\bq}|\,, & k&\sim(v_\infty,v_\infty)|{\bq}|\,.
		  \end{align}$$
		  Choose a frame \(u_1=(1,0,0,0)\,, u_2=(\gamma, 0,0,v_\infty)\), effectively:
		  $$\begin{align}
		  &\mathrm{potential:} \quad (\bell\to \bell, \bk\to \bk)\,,\\ 
		  &\mathrm{radiation:}  \quad (\bell\to \bell, \bk\to v_\infty \tilde\bk)\,,\quad \tilde\bk\sim \bq\,,\label{res3pm}
		  \end{align}$$
		  Leading to
		  $$\begin{align}
		  I_{1}^\mathrm{pot} &= -\int_{\bell,\bk} \frac{1}{[(\bk-\bell+\bq)^2]\, [\bell^2]\, [\bk^2]} + \cO(v_\infty^2)\,,
		  \\
		  I_{1}^\mathrm{rad} &= - \int_{\bell}  \frac{1}{  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\tilde \bk} \frac{v_\infty^{d-2} }{  [\tilde\bk^2 - (\ell^z)^2] }
		  +\cO(v_\infty^{d})\,,
		  \end{align}$$
		</p>
	  </div>

	  <div id="int6" class="step" data-rel-y="550" data-rel-x="0">
		<hr/>
		<p>
		  Feynman vs. causal
		  $$\begin{align}
		  I_{1,\rm Fey}^\mathrm{rad} &= - \int_{\bell}  {1  \over  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\bk} {v_\infty^{d-2} \over  [\bk^2 - (\ell^z)^2 - i0] }
		  +\cO(v_\infty^{d})\,,
		  \\[0.35 em]
		  I_{1,\rm ret}^\mathrm{rad} &= - \int_{\bell}  {1  \over  [(\bell - \bq)^2]\,[\bell^2] }\,
		  \int_{\bk} {v_\infty^{d-2} \over  [\bk^2 - (\ell^z + i0)^2] }
		  +\cO(v_\infty^{d})\,.
		  \end{align}$$
		</p>
		<p class="center"><img id="I1PNfig" src="figures/I1PN.png"></img></p>
	  </div>

	  <div id="int8" class="step" data-rel-y="600" data-rel-x="0">
		<h3><span class="h3span">Meth<span class="orange">o</span>d of regions @4PM</span></h3>
		<a class="ref" href="https://arxiv.org/abs/2304.01275">[Dlapa et al. 2304.01275]</a><br/>
		<p class="center"><img id="I2fig" src="figures/I2.png"></img></p>
		<p>
		  $$I_2= \int_{\ell_1,\ell_2,\ell_3}  \frac{ \delta (\ell_1\!\cdot\! u_1)\, \delta (\ell_2\!\cdot\! u_1)\, \delta(\ell_3\!\cdot\! u_2)} {\ell_1^2\,\ell_3^2\,(\ell_2 {-}q)^2\, (\ell_3 {-} q)^2\, (\ell_1 {-} \ell_2)^2\, (\ell_2 {-} \ell_3)^2\, (\ell_3{-}\ell_1)^2}$$
		  Relabel \(k_1=\ell_3-\ell_1\), \(k_2=\ell_2-\ell_3\), \(\ell = \ell_3\). Regions:
		  $$\begin{align}
  &\mathrm{pot:} & k_1&\sim(v_\infty,1)|\bq|\,, & k_2&\sim(v_\infty,1)|\bq|\,, & \ell&\sim(v_\infty,1)|\bq|\,,\\
  &\mathrm{1rad}^{(1)}: & k_1&\sim(v_\infty,v_\infty)|\bq|\,, & k_2&\sim(v_\infty,1)|\bq|\,, & \ell&\sim(v_\infty,1)|\bq|\,,\\
  &\mathrm{1rad}^{(2)}: & k_1&\sim(v_\infty,1)|\bq|\,, & k_2&\sim(v_\infty,v_\infty)|\bq|\,, & \ell&\sim(v_\infty,1)|\bq|\,,\\
  &\mathrm{2rad:} & k_1&\sim(v_\infty,v_\infty)|\bq|\,, & k_2&\sim(v_\infty,v_\infty)|\bq|\,, & \ell&\sim(v_\infty,1)|\bq|\,,
  \end{align}$$
		</p>
	  </div>

	  <div id="int9" class="step" data-rel-y="660" data-rel-x="0">
		<hr/>
		<p>
		  Expand in these regions
		  $$\begin{align}
I_{2}^\textrm{pot} &= \int_{\bell,\bk_1,\bk_2} {1 \over [(\bell-\bk_1)^2]\, [\bell^2]\, [(\bk_2+\bell-\bq)^2]\, [(\bell-\bq)^2]\, [(\bk_1+\bk_2)^2]\, [\bk_2^2]\, [\bk_1^2]} +\cO(v_\infty^2)\,,
\\
%%
I_{2}^{\textrm{1rad}(1)} &= \int_{\bell,\bk_2} {1 \over [\bell^2]\, [\bell^2]\, [(\bk_2+\bell-\bq)^2]\, [(\bell-\bq)^2]\, [\bk_2^2]^2}
\int_{\bk_1} {v_\infty^{d-2}  \over \bk_1^2 - (\ell^z)^2} +\cO(v_\infty^{d})\,,
\\
%%
I_{2}^{\textrm{1rad}(2)} &= \int_{\bell,\bk_1} {1 \over [(\bell-\bk)^2]\, [\bell^2]\, [(\bell-\bq)^2]\, [(\bell-\bq)^2]\, [\bk_1^2]^2}
\int_{\bk_2} {v_\infty^{d-2}  \over \bk_2^2 - (\ell^z)^2} +\cO(v_\infty^{d})\,,
\\
%%
I_{2}^\textrm{2rad} &=  \int_\bell {1  \over  [\bell^2] \,[\bell^2]\, [(\bell-\bq)^2]^2}
\int_{\bk_1,\bk_2} {v_\infty^{2d-6} \over  [(\bk_1+\bk_2)^2]\, [\bk_2^2 - (\ell^z)^2]\, [\bk_1^2 - (\ell^z)^2]} +\cO(v_\infty^{2d-4})\,.
\end{align}$$
		</p>
		<ul>
		  <li>Dress with Feynman (conservative) or ret/adv (RR) \(i0\).</li>
		  <li>All regions added up lead to a finite result.</li>
		  <li>Perform inner integral first: Outer integral becomes lower-loop potential integral.</li>
        </ul>
	  </div>

	  <div div="int10" class="step" data-rel-y="445" data-rel-x="0">
		  <ul>
			<li>rad2 corresponds to PN tail-type integrals
			<p class="center">
			  <img id="l2PNFig" src="figures/l2PN.png" alt="l2PN"></img>
		</p>
			</li>
		  </ul>
	  </div>

	  <div id="Dpres" class="step" data-rel-x="0" data-rel-y="600">
		<h2>Results up t<span class="orange">o</span> 4PM</h2>
		<a class="ref" href="https://arxiv.org/abs/2210.05541">[Dlapa, GK, Liu, Neef, Porto 2210.05541]</a><br/>
		<div class="center">
		  $$\Delta^{(n)} p_1^\mu =c^{(n)}_{1b}\, \hat b^\mu +  \sum_{a} c^{(n)}_{1\check{u}_a}\, \check{u}_a^\mu$$
		  <img id="res12PMFig" src="figures/res12PM.png" alt="res12PM"/>
		</div>
	  </div>

	  <div id="Dpres1.5" class="step" data-rel-x="0" data-rel-y="520">
		<div class="center">
		  <img id="res3PMFig" src="figures/res3PM.png" alt="res3PM"/>
		</div>
	  </div>

	  <div id="Dpres1.9" class="step" data-rel-x="0" data-rel-y="700">
		<div class="center">
		  <img id="res4PMFig" src="figures/res4PM.png" alt="res4PM"/>
		</div>
	  </div>

	  <div id="integrals2" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="integrals">
	  </div>

	  <div id="integralsEnd" class="step">
		<h3><span class="h3span">Challenge 2: (Feynman) Integrals</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Analytic representation. <b>New challenge at every loop order!</b></li>
			<li>vs. fast numerical evaluation. <b>Worth thinking about.</b></li>
			<li>Minimal function space. <b>Not very relevant (until now).</b></li>
			<li>Bootstrap. <b>Be creative</b></li>
		  </ul>
		</blockquote>
	  </div>

	  <div id="B2BChallenge" class="step" data-rel-x="1700">
		<h3><span class="h3span">Challenge 3: Bound orbits and resummations.</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Completing the boundary-to-bound dictionary.</li>
			<li>Performing and understanding resummations.</li>
			<li>Apply amplitudes methods directly to bound problem.</li>
		  </ul>
		</blockquote>
	  </div>

	  <div id="B2B" class="step" data-rel-x="0" data-rel-y="900">
		<h3><span class="h3span">B<span class="orange">o</span>undary-To-Bound (B2B)</span></h3>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1911.09130">[1911.09130, w/ Porto]</a>
		<a class="ref" href="http://arxiv.org/abs/arXiv:1910.03008">[1910.03008, w/ Porto]</a>
		Use scattering data, e.g. the scattering angle
		$$\chi(b,E) +\pi = 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}}$$
		to obain bound observables, e.g. the periastron advance:
		$$\begin{align}
		  \Delta \Phi + 2\pi =  2J \int_{r_-=r_{\rm min}(J)}^{r_+=r_{\rm min}(-J)} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}
		  &=2J \int_{r_\textrm{min}(J)}^{\infty} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}\\
		  &\quad-2J \int_{r_\textrm{min}(-J)}^{\infty} \frac{\dd r}{r\sqrt{r^2\bp^2(r,E)-J^2}}\\
		  &=\chi(J,E)+\chi(-J,E)+2\pi
		\end{align}$$
		Don't forget: \(J=p_\infty b\), \(\bar\bp = \bp/p_\infty\).
	  </div>

	  <div id="B2B2" class="step" data-rel-x="0" data-rel-y="650">
	  <h3><span class="h3span">Local vs. n<span class="orange">o</span>n-local for circular orbits</span></h3>
	  <a class="ref" href="http://arxiv.org/abs/arXiv:2106.08276">[2106.08276]</a>
	    Non-local contributions to unbound radial action
	  $$\cI_{r\textrm{(nloc)}}^{\textrm{4PM}}=-\frac{E}{2\pi M^2\nu}\left(\int \frac{\dd\omega}{2\pi}\frac{\dd E}{\dd\omega}\log(4\omega^2e^{2\gamma_E})+\int \dd t\frac{\dd E}{\dd t}\log(r^2(t))\right)$$
	  Not all terms go through above B2B dictionary.
	  <blockquote class="box">
	  <b>Challenge:</b><br/> Remove non-local terms from unbound quantities & evaluate them instead on bound orbits.
	  </blockquote>
	  </div>
	  
	  <div id="B2B3" class="step" data-rel-x="0" data-rel-y="550">
		<h3><span class="h3span">Radiati<span class="orange">o</span>n</span></h3>
		
		<a class="ref" href="https://arxiv.org/abs/2112.03976">[2112.03976, w/ Cho, Porto]</a>
		<p class="margin-top60">
		In a similar way we can analytically continue radiative observables, e.g. the energy and angular momentum loss
		$$\begin{align}
		\Delta E_\textrm{ell}(J,\cE) &= \Delta E_\textrm{hyp}(J,\cE)-\Delta E_\textrm{hyp}(-J,\cE)\\
		\Delta J_\textrm{ell}(J,\cE) &= \Delta J_\textrm{hyp}(J,\cE)+\Delta J_\textrm{hyp}(-J,\cE)\\
		\end{align}$$
		</p>
	  </div>

	  <div id="Firsov" class="step" data-rel-x="0" data-rel-y="680">
		<h2>Firs<span class="orange">o</span>v's formula</h2>
		<a class="ref" href="https://arxiv.org/abs/1910.03008">[w/ Porto 1910.03008]</a><br/>
		Let us invert
		$$\chi(b,E) = -\pi + 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}} = \sum_{n=1} \chi^{(n)}_b(E) \left(\frac{GM}{b}\right)^n $$
		<a class="inlineRef" href="">[Firsov '53]</a>: dependence on \(r_\textrm{min}\) drops out
		<div class="center"><div class="centerBox">
			$$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right] = 1 + \sum_{n=1}^\infty f_n(E) \left(\frac{GM}{r}\right)^n$$
		</div></div>
		These integrals are easy to perform in a PM-expanded form and one finds:
		$$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		The inversion thereof also exists.
	  </div>

	  <div id="Dpres2" class="step" data-rel-y="750">
		<h3><span class="h3span">Firsov resummation </span></h3>
<div class="center">
		  <p class="inlineRef">Resummation and comparison to NR <a href="https://arxiv.org/abs/2211.01399">[Damour, Rettegno 2211.01399]</a></p>
		  <p> Equal mass scattering.</p>
		  <img id="chiEOBFig" src="figures/chiEOB.png"></img>
		</div>
	  </div>

	  <div id="Dpres3" class="step" data-rel-y="700" data-rel-to="Dpres2">
		<div class="center">
		  <img id="chiEOB2Fig" src="figures/chiEOB2.png"></img>
		</div>
	  </div>

	  <div id="B2BChallenge2" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="B2BChallenge">
	  </div>

	  <div id="B2BChallengeEnd" class="step" data-rel-x="0" data-rel-y="0" data-rel-to="B2BChallenge">
		<h3><span class="h3span">Challenge 3: Bound orbits and resummations.</span></h3>
		<blockquote class="box">
		  <ul>
			<li>Completing the boundary-to-bound dictionary. <b>Nonlocal. Spin. Waveform dictionary?</b></li>
			<li>Performing and understanding resummations. <b>Why so good? What about bound?</b></li>
			<li>Apply amplitudes methods directly to bound problem. <b>Challenge for all of you!</b></li>
		  </ul>
		</blockquote>
	  </div>

	   <div id="conclusions" class="step" data-rel-x="1700" data-rel-y="0">
		<h2>Conclusi<span class="orange">o</span>ns</h2>
		<ul>
		  <li>PMEFT: systematic and efficient framework to study the gravitational 2-body dynamics.</li>
		  <li>Modern integration techniques work for PM integrals with retarded propagators.</li>
		  <li>Complete answer, including radiation-reaction, for the impulse/deflection up to 4PM.</li>
		  <li>Resummed results agree well with numerical relativity simulations.</li>
		  <li>Non-local B2B remains an important challenge to fulfill our promises to GW community.</li>
		  <li>Need improved tools for higher orders.</li>
		</ul>
		<blockquote class="box">
		  <b>Time to face these challenges!</b>
		</blockquote>
	  </div>

	  <div id="final" class="step" data-rel-x="0" data-rel-y="660">
		<p class="center">
		  <img id="mainFig" src="figures/main.svg" alt="scattering">
		</p>
		<p class="small">
		  This research is supported by the ERC-CoG “Precision Gravity: From the LHC to LISA” provided by the European Research Council (ERC) under the European Union’s H2020 research and innovation programme (grant No. 817791), by the DFG under Germany’s Excellence Strategy ‘Quantum Universe’ (No. 390833306).
		</p>
	  </div>

	  <div id="highEnergy" class="step" data-rel-x="1700" data-rel-y="0" data-rel-to="conclusions">
		<h2>High Energy</h2>
		  <li>High-energy + massless limit is divergent. Keeping \(s\equiv \gamma m_1 m_2\) fixed:
			$$\begin{align}
			c_{1b}^{(4)\mathrm{tot}} &\sim \frac{s^3}{m_1} + \cO(m_i)\\
			\frac{\Delta E^{\mathrm{4PM}}_{\textrm{hyp}}}{\sqrt{s}} &\sim \frac{G^4 s^2}{b^4} \log\left(\frac{s}{m_1 m_2}\right)+\cO(m_i)
			\end{align}$$
		  </li>
        </ul>
	  </div>

	  <div id="Firsov1" class="step" data-rel-x="1700" data-rel-y="0">
		<h2>PM to PN: The Magic of Firsov</h2>
		<img id="firsovFig" src="figures/Firsov.jpg" alt="firsov">
		Let's consider a simple example
		$${\Delta \Phi}(j,\cE)= \sum_{n=1} \Delta \Phi_j^{(2n)}(\cE)/j^{2n}$$
		with \(j=J/(G M \mu)\). B2B states:
		$$\Delta \Phi_j^{(2n)}(\cE)= 4\, \chi^{(2n)}_j(\cE)$$
		Let's assume we don't know anything about \(\chi^{(4)}_j\). Does this mean we don't know anything about \(\Delta \Phi_j^{(4)}\)? There's lot of lower PN information that our PM results up to \(\chi_j^{(3)}\) should contain. Where are they?
	  </div>
	  
	  <div id="Firsov2" class="step" data-rel-x="0" data-rel-y="680">
		<h3><span class="h3span">Firs<span class="orange">o</span>v's formula</span></h3>
		Let us invert
		$$\chi(b,E) = -\pi + 2b \int_{r_{\rm min}}^\infty \frac{\dd r}{r\sqrt{r^2\bar\bp^2(r,E)-b^2}} = \sum_{n=1} \chi^{(n)}_b(E) \left(\frac{GM}{b}\right)^n $$
		<a class="inlineRef" href="">[Firsov '53]</a>: dependence on \(r_\textrm{min}\) drops out
		<div class="center"><div class="centerBox">
			$$\bar\bp^2(r,E) = \exp\left[ \frac{2}{\pi} \int_{r|\bar{\bp}(r,E)|}^\infty \frac{\chi(\tilde b,E)\dd\tilde b}{\sqrt{\tilde b^2-r^2\bar\bp^2(r,E)}}\right] = 1 + \sum_{n=1}^\infty f_n(E) \left(\frac{GM}{r}\right)^n$$
		</div></div>
		These integrals are easy to perform in a PM-expanded form and one finds:
		$$\chi_b^{(n)} = \frac{\sqrt{\pi}}{2} \Gamma\left(\frac{n+1}{2}\right)\sum_{\sigma\in\mathcal{P}(n)}\frac{1}{\Gamma\left(1+\frac{n}{2} -\Sigma^\ell\right)}\prod_{\ell} \frac{f_{\sigma_{\ell}}^{\sigma^{\ell}}}{\sigma^{\ell}!}$$
		The inversion thereof also exists.
	  </div>

	  <div id="Firsov3" class="step" data-rel-x="0" data-rel-y="650">
		<h3><span class="h3span">Back to our pr<span class="orange">o</span>blem</span></h3>
		$$\frac{1}{4}\Delta \Phi_j^{(4)}= \chi^{(4)}_j = \left(\frac{p_\infty}{\mu}\right)^4\chi_b^{(4)}=\left(\frac{p_\infty}{\mu}\right)^4\frac{3\pi}{16}(2f_1f_3+f_2^2+2f_4)$$
		and in turn
		$$\begin{align}
		f_1&=2\chi_b^{(1)}\\
		f_2&=\frac{4}{\pi}\chi_b^{(2)}\\
		f_3&=\frac{1}{3}\left(\chi_b^{(1)}\right)^3+\frac{4}{\pi}\chi_b^{(1)}\chi_b^{(2)}+\chi_b^{(3)}
		\end{align}$$
		Prefectly reproduces 1 and 2PN information at order \(j^{-4}\).
	  </div>

	  <div id="numInt1" class="step" data-rel-y="0" data-rel-x="1700" data-rel-to="Firsov1">
		<h2>Numerical integration using machine learning</h2>
		<a class="ref" href="https://arxiv.org/abs/2209.01091">[Jinno, GK, Liu, Rubira 2209.01091]</a>
		<ul>
		  <li>Why numerical integration?</li>
		  <ul>
			<li>Cross-checks are important and incredibly useful</li>
			<li>Might be the only available method at higher loops</li>
			<li>Hybrid: Analytical bootstrap</li>
		  </ul>
		  <li>Need fast algorithms for high precision!</li>
		  <li>Our idea: teach a neural network to do the Monte-Carlo integration making use of the <em>normalizing flows</em> technology.</li>
		</ul>		
	  </div>

	  <div id="numInt2" class="step" data-rel-y="550" data-rel-x="0">
		<ul>
		  <li><em>Importance Sampling:</em> Pick points for Monte-Carlo evaluation such that regions of large integrand \(|f|\) gain more weight.</li>
		  <li>I.e. take a distribution \(x(G)\), \(\dd G = g(x) \dd x\)
			$$I = \int_\Omega \dd x f(x) = \int_{\tilde\Omega} \dd G \frac{f(x(G))}{g(x(G))}$$
			that minimizes the variance
			$$\sigma^2=\frac{1}{N-1}\left[\frac{1}{N}\sum_i \left(\frac{f(G_i)}{g(G_i)}\right)^2-\left(\frac{1}{N}\sum_i \frac{f(G_i)}{g(G_i)}\right)^2\right]$$
		  </li>
		  <li>Hence, optimally \(g(x) = f(x)/I\), but it should also be invertible and fast to evaluate.</li>
		</ul>
	  </div>

	  <div id="numInt3" class="step" data-rel-y="650" data-rel-x="0">
		<ul>
		  <li>Traditional Monte-Carlo algorithms assume \(g(x_1,\dots,x_n) = g(x_1) \dots g(x_n)\) and then use an \(N\)-dimensional histogram for each dimension.</li>
		  <ul>
			<li>Often not true at all and leads to a bad sampling.</li>
		  </ul>
		  <li>Better: Machine learning in the form of a neural network setup</li>
		  <div class="center">
			<img id="NNFig" src="figures/NN.png" alt="NN"/>
			$$J =\left|\begin{pmatrix} 1 & 0 \\ \frac{\partial C}{\partial m}\frac{\partial m}{\partial x_A} & \frac{\partial C}{\partial x_B}\end{pmatrix}\right| = \left| \frac{\partial C}{\partial x_B} \right|$$
		  </div>
		</ul>
	  </div>
	  
	   <div id="numInt4" class="step" data-rel-y="800" data-rel-x="0">
		 <ul>
		   <li>Promising results for (potential) boundary integrals up to 4-loops</li>
		   <div class="center">
			 <img id="NNplots1Fig" src="figures/NNplots1.png" alt="NN1"/>
			 <img id="NNplots2Fig" src="figures/NNplots2.png" alt="NN2"/>
		   </div>
		 </ul>
	   </div>
	  
  	</div>

	<div id="impress-toolbar"></div>

	<!--
	<div class="hint">
      <p>Use a spacebar or arrow keys to navigate. <br/>
		Press 'P' to launch speaker console.</p>
	</div>
	-->
	<script>
	  if ("ontouchstart" in document.documentElement) { 
		  document.querySelector(".hint").innerHTML = "<p>Swipe left or right to navigate</p>";
	  }
	  </script>

	<script src="js/impress.js"></script>
	<script>impress().init();</script>

  </body>
</html>
